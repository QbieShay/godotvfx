shader_type spatial;
render_mode unshaded;
render_mode skip_vertex_transform;

uniform float pixel_to_world = 512.0;
uniform sampler2D crack_texture;
varying vec3 pos;
varying vec3 dir;

void vertex() {
	// get our pos on the surface of our mesh in model space
	
	// make our vertex position
	VERTEX = (MODELVIEW_MATRIX * vec4(VERTEX, 1.0)).xyz;
	pos = VERTEX.xyz;
	dir = normalize(pos);
	dir = (inverse(MODELVIEW_MATRIX)*vec4(pos,0.0)).xyz;
	mat3 tangent = mat3(TANGENT,BINORMAL,NORMAL);
	dir = tangent*dir;
//	VERTEX = (PROJECTION_MATRIX*vec4(VERTEX, 1.0)).xyz;
}


vec3 march(vec2 coordinates, vec3 view_dir){
	float i;
	for(i = 0.0;i < float(100);i +=0.01){
		if(texture(crack_texture, fract(coordinates + i*vec2(view_dir.x,-view_dir.y))).r > 0.3){
			break;
			}
	}
	return view_dir*i;
}

void fragment() {
	vec3 color = texture(crack_texture, UV).rgb;
	if (color.r > 0.3){
		ALBEDO = color;
	}else{
	ALBEDO = mix(
		vec3(1.0,0.0,0.0),
		vec3(0.0,0.0,1.0),
		-(march(UV,dir).z));
	}
}
